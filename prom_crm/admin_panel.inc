<?php

function prom_crm_my_admin ($content = NULL) {
  $base_content ='<b>'. t('From here you can see the state of all opened questions and review old ones.').'</b><br /><br />' .
                  t('Go to ') . l(t('Statistics'), 'my_admin/statistics') . t(' to view more information about questions;'). '<br />'.
                  t('Go to ') . l(t('To Assign'), 'my_admin/to_assign') . t(' to view questions waiting for  assignment;'). '<br />'.
                  t('Go to ') . l(t('Open Question'), 'my_admin/open') . t(' to see and manage all open questions;'). '<br />'.
                  t('Go to ') . l(t('Closed Question'), 'my_admin/closed') . t(' to view all closed questions.');
  return $base_content;
  
}


function prom_crm_to_assign_admin ( $content = NULL){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Questioner'), 'field' => 'uid_author_question'),
  array('data' => t('Open Date'), 'field'=> 'sqldate'));
  $data = array();
  $countrows= db_result(db_query("SELECT COUNT(*) FROM {prm} WHERE status = 'open' AND assignee=0"));
  if($countrows > 0){
    $query = "SELECT questid, type, title, content, sqldate, uid_author_question FROM {prm} WHERE status = 'open' AND assignee=0" .
             tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $limit = variable_get('prom_crm_rows_admin', 10);
    $result = pager_query($query, $limit, 0, $count_query, NULL);
    while($temp2 = db_fetch_array($result)){
      $load_questioner = user_load($temp2['uid_author_question']);
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'], 'my_admin/to_assign/' . $temp2['questid'] . '/display'),
      $load_questioner->name,
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));
    }
    $output = theme_table($header, $data);
    $output .= theme('pager', NULL, $limit, 0); 
    return $output; 
  }
  else{
      return '<br /><strong>' . t('There\'s no question to assign') . '</strong>';
  } 
}

function prom_crm_statistics($content = NULL){
  $countrows1= db_result(db_query("SELECT COUNT(questid) FROM {prm}"));
  if ($countrows1 == 0){
    $output = '<br /><b>' . t('There is no question.') . '</b><br />';
  }
  else{
    $output = '<br /><b>' . t('Total number of questions: ') .'</b>' . $countrows1 . '<br />';
    
    $countrows2 = db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status= 'open' AND assignee != 0"));
    $output .= '<br /><b>' . t('Number of open questions: ').'</b>' . $countrows2 . '<br />';
    
    $countrows3= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status= 'closed'"));
    $output .= '<br /><b>' . t('Number of closed questions: ').'</b>' . $countrows3 . '<br />';
    
    $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE assignee = 0 AND status = 'open'"));
    $output .= '<br /><b>' . t('Number of questions waiting for an assignment: ').'</b>' . $countrows . '<br />';
    
    $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE assignee != 0 AND uid_author_last_post = uid_author_question AND status = 'open'"));
    $output .= '<br /><b>' . t('Number of questions waiting for assignee reply: ').'</b>' . $countrows . '<br />';
      
    $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE assignee != 0 AND uid_author_last_post != uid_author_question AND status = 'open'"));
    $output .= '<br /><b>' . t('Number of questions waiting for questioner reply: ').'</b>' . $countrows . '<br />';
      
    $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status = 'closed' AND uid_author_last_post != assignee AND uid_author_last_post != uid_author_question"));
    $output .= '<br /><b>' . t('Number of questions closed by admin: ').'</b>' . $countrows . '<br />';  
    $result = db_query("SELECT numscambio FROM {prm}");
    $countrows = 0;
    while($temp = db_fetch_array($result)){
      $countrows += $temp[numscambio];
    }
    if ($countrows > 0){
      $output .= '<br /><b>' . t('Average number of interaction between assignee and questioner: ').'</b>' . round($countrows/$countrows1,2) . '<br />';     
    }
    $countrows = 0;
    if($countrows2 > 0){  
      $result = db_query("SELECT numscambio FROM {prm} WHERE status = 'open' AND assignee != 0");
      while($temp = db_fetch_array($result)){
        $countrows += $temp[numscambio];
      }
      if ($countrows > 0){
        $output .= '<br /><b>' . t('Average number of interaction between assignee and questioner in open questions: ').'</b>' . round($countrows/$countrows2,2) . '<br />';     
      }
    }
    $countrows = 0;
    if($countrows3 > 0){  
      $result = db_query("SELECT numscambio FROM {prm} WHERE status = 'closed'");
      while($temp = db_fetch_array($result)){
        $countrows += $temp[numscambio];
      }
      if ($countrows > 0){
        $output .= '<br /><b>' . t('Average number of interaction between assignee and questioner in closed questions: ').'</b>' . round($countrows/$countrows3,2) . '<br />';     
      }
    }
  }
  return $output;
}
function prom_crm_open_admin($content = NULL){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Questioner'), 'field' => 'uid_author_question'),
  array('data' => t('Assignee'), 'field' => 'assignee'),
  array('data' => t('Open Date'), 'field'=> 'sqldate'),
  array('data' => t('Last Reply'), 'field' => 'tp'));
  $data = array();
  $output;
  $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status= 'open' AND assignee !=0 AND uid_author_question = uid_author_last_post"));
    if($countrows > 0){
    $query = "SELECT questid, type, title, content, sqldate, uid_author_question, uid_author_last_post, assignee, tp FROM {prm} WHERE status = 'open' AND assignee !=0 AND uid_author_question = uid_author_last_post" .
             tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $limit = variable_get('prom_crm_rows_admin', 10);
    $sql_args = array();
    $result = pager_query($query, $limit, 0, $count_query, $sql_args);
    while($temp2 = db_fetch_array($result)){
      $load_questioner = user_load($temp2['uid_author_question']);
      $load_assignee = user_load($temp2['assignee']);
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_admin/open/' . $temp2['questid'] . '/display'),
      $load_questioner->name,
      $load_assignee->name,
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'),
      format_date($temp2['tp'],'custom', 'd/m/Y - G:i'));
    }
    $output .= theme_table($header, $data);
    $output .= theme('pager', NULL, $limit, 0); 
  }
  else{
    $output .= '<br /><strong>' . t('there is no open question to display') . '</strong>';
    return $output;
  }
  return $output;
  
}


function my_admin_open_questioner ($content = NULL){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Questioner'), 'field' => 'uid_author_question'),
  array('data' => t('Assignee'), 'field' => 'assignee'),
  array('data' => t('Open Date'), 'field'=> 'sqldate'),
  array('data' => t('Last Reply'), 'field' => 'tp'));
  $data2 = array();
   $output;
   $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status= 'open' AND assignee !=0 AND uid_author_question != uid_author_last_post"));
   if($countrows > 0){
   $query = "SELECT questid, type, title, content, sqldate, uid_author_question, uid_author_last_post, assignee, tp FROM {prm} WHERE status = 'open' AND assignee !=0 AND uid_author_question != uid_author_last_post" .
            tablesort_sql($header);
   $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
   $limit = variable_get('prom_crm_rows_admin', 10);
   $sql_args;
   $result = pager_query($query, $limit, 0, $count_query, $sql_args);
    while($temp2 = db_fetch_array($result)){
      $load_questioner = user_load($temp2['uid_author_question']);
      $load_assignee = user_load($temp2['assignee']);
      $data2[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_admin/open/' . $temp2['questid'] . '/display'),
      $load_questioner->name,
      $load_assignee->name,
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'),
      format_date($temp2['tp'],'custom', 'd/m/Y - G:i'));
    }
    if(!empty($data2)){
      $output .= '<br />' . t('View all open questions waiting for the questioner action');
      $output .= theme_table($header, $data2);
      $output .= theme('pager', NULL, $limit, 0);
    }
    else {
      $output .= '<br /><strong>' . t('There is no question waiting for the questioner action') . '</strong>';
    }
    return $output;
  }
  else{
    $output .= '<br /><strong>' . t('there is no open question to display') . '</strong>';
    return $output;
  }
}


function  prom_crm_closed_admin($content = NULL){
 global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Questioner'), 'field' => 'uid_author_question'),
  array('data' => t('Assignee (*)'), 'field' => 'assignee'),
  array('data' => t('Open Date'), 'field'=> 'sqldate'),
  array('data' => t('Close Date'), 'field' => 'closedate'));
  $data = array();
  $output;
  $countrows= db_result(db_query("SELECT COUNT(*) FROM {prm} WHERE status='closed'"));
  if($countrows > 0){
    $query = "SELECT questid, type, title, content, sqldate, uid_author_question, assignee, closedate FROM {prm} WHERE status = 'closed'" .
             tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $limit = variable_get('prom_crm_rows_admin', 10);
    $result = pager_query($query, $limit, 0, $count_query, NULL);
    while($temp2 = db_fetch_array($result)){
      $load_questioner = user_load($temp2['uid_author_question']);
      $load_assignee = user_load($temp2['assignee']);
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_admin/closed/' . $temp2['questid'] . '/display'),
      $load_questioner->name,
      $load_assignee->name,
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'),
      format_date($temp2['closedate'],'custom', 'd/m/Y - G:i'));
    }
    $output .= '<br />' . t('View all closed questions') . '<br />';
    $output .= theme_table($header, $data);
    $output .= theme('pager', NULL, $limit, 0);
    $output .= '<br /><sub>' . t('(*) If the field is blank, the question has been closed before the assignment') . '</sub><br />';
    
    return $output; 
  }
  else{
    $output .= '<strong>'. t('There is no closed question.') . '</strong><br />';
  }
  return $output;
}


function myadmin_toassign_display($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account = $user;
  //verify if there are question
  $countrows = db_result(db_query("SELECT COUNT(*) FROM {prm} WHERE status = 'open' AND assignee=0 AND questid = %d", $content));
  $output;
  if($countrows > 0){
    $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'open' AND assignee=0 AND questid = %d", $content);
    $result = db_fetch_array($temp);
    $node = node_load($result['nid'], NULL, TRUE);
    $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br /><div><b>Content: </b>' . $node->body .  '</div><br /><div><b>Question: </b>' . $result['content'] . '</div><br />'; 
    return $output . drupal_get_form('prom_crm_admin_toassign_form', $content);
  }
  else{
    return  t('There was a problem during the request.');
  }
}


function prom_crm_admin_toassign_form(&$form_state, $arg1){
  $form['quest_id'] = array('#type' => 'hidden', '#value' => $arg1);
  $form['back']     = array('#type' => 'submit', '#value' => t('Back'));
  $form['submit']   = array('#type' => 'submit', '#value' => t('Close this question'));
  return $form;
}


function prom_crm_admin_toassign_form_submit(&$form_state, $form){
  global $user;
  $account = $user;
  $form_values = $form['values'];
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_admin/to_assign');
  }
  else{
  db_query("UPDATE {prm} SET status = 'closed', closedate = %d, tp = %d, uid_author_last_post =%d WHERE questid = %d", time(), time(), $account->uid, $form_values['quest_id'] );
  $message = t('The question has been closed.');
  $mail_send = variable_get('prom_crm_mail_sent', array());
  if ($mail_send['admin_close'] == 'yes'){
    prom_crm_admin_close_mail_send($form_values);
  }
  drupal_set_message($message);
  drupal_goto('my_admin/closed');
  }
}

function prom_crm_admin_close_mail_send($form_values) {
  $module = 'prom_crm';
  $key = 'admin_close';
  $params = $form_values;
  $language = language_default();
  $send = TRUE;
  $questioner = db_result(db_query("SELECT uid_author_question FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  $load_questioner = user_load($questioner);
  $to = $load_questioner->mail;
  $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  $assignee = db_result(db_query("SELECT assignee FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  if($assignee != 0){
    $load_assignee = user_load($assignee);
    $to = $load_assignee->mail;
    $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  }
}

function prom_crm_myadmin_open_display($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account == $user;
  $output;
  $countrows = db_result(db_query("SELECT COUNT(*) FROM {prm} WHERE status = 'open' AND questid = %d AND assignee !=0", $content));
  $output;
  if($countrows > 0){
    $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'open' AND questid = %d AND assignee !=0", $content);
    $result = db_fetch_array($temp);
    $node = node_load($result['nid'], NULL, TRUE);
    $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
    $array_explode=explodeX(array("#####"),$result['content']);
    $num = 0;
    foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<br /><b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
    $output .= t('Choose to send a reply or to close the question.');    
    return $output . drupal_get_form('prom_crm_myadmin_open_display_form', $content);
  }
  else {
    return t('There was a problem during the request');
  }
}


function prom_crm_myadmin_open_display_form(&$form_state, $arg1){
  $form['quest_id'] = array('#type' => 'hidden', '#value' => $arg1);
  $form['reply']    = array('#type' => 'textarea','#title' => t('Reply'),'#required' => FALSE);
  $form['back']   = array('#type' => 'submit', '#value' => t('Back'));
  $form['close']   = array('#type' => 'submit', '#value' => t('Close the question')); 
  $form['submit']   = array('#type' => 'submit', '#value' => t('Send a reply without closing'));
  return $form;
}


function prom_crm_myadmin_open_display_form_submit(&$form_state, $form){
  global $user;
  $account = $user;
  $form_values = $form['values'];
  $mail_send = variable_get('prom_crm_mail_sent', array());
  
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_admin/open');
  }
  //if the questioner has clicked on 'submit'
  if($form['clicked_button']['#value'] == $form['values']['submit']){
    if ($form_values['reply'] != '' &&  strpos($form_values['reply'], '#####') === FALSE){
      $message = db_result(db_query('SELECT content FROM {prm} WHERE questid=%d', $form_values['quest_id']));
      $message .= ' #####ADMIN##### ' . $form_values['reply'];
      db_query("UPDATE {prm} SET content = '%s',  tp = %d WHERE questid = %d", $message, time(), $form_values['quest_id'] );
      if ($mail_send['admin_answer'] == 'yes'){
        prom_crm_admin_reply_mail_send($form_values);
      }
      drupal_set_message(t('The reply has been sent.'));
      drupal_goto('my_admin/open');
    }
    else {
      drupal_set_message(t('You must write a reply before.'), 'error');
    }
  }
  //if the questioner has click on 'close'
  if($form['clicked_button']['#value'] == $form['values']['close']){
    $message = db_result(db_query('SELECT content FROM {prm} WHERE questid=%d', $form_values['quest_id']));
    $message .= ' #####ADMIN##### ' . $form_values['reply'];
    db_query("UPDATE {prm} SET content ='%s', status = 'closed', closedate = %d, tp = %d  WHERE questid = %d", $message, time(), time(), $form_values['quest_id']);
    if ($mail_send['admin_close'] == 'yes'){
      prom_crm_admin_close_mail_send($form_values);
    }
    drupal_set_message(t('The question has been closed'));
    drupal_goto('my_admin/closed');
  }
}

function prom_crm_admin_reply_mail_send($form_values) {
  $module = 'prom_crm';
  $key = 'admin_answer';
  $params = $form_values;
  $language = language_default();
  $send = TRUE;
  $questioner = db_result(db_query("SELECT uid_author_question FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  $load_questioner = user_load($questioner);
  $to = $load_questioner->mail;
  $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  $assignee = db_result(db_query("SELECT assignee FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  $load_assignee = user_load($assignee);
  $to = $load_assignee->mail;
  $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
}

function admin_closed_display ($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account = $user;
  $output;
  $countrows = db_result(db_query("SELECT COUNT(*) FROM {prm} WHERE status = 'closed' AND questid = %d", $content));
  $output;
  if($countrows > 0){
    $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'closed' AND questid = %d", $content);
    $result = db_fetch_array($temp);
    $node = node_load($result['nid'], NULL, TRUE);
    $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
    $array_explode=explodeX(array("#####"),$result['content']);
    $num = 0;
    foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
  }
  else{
    $output .= t('There was a problem during the request');
    return $output;
  }
  return $output . drupal_get_form('admin_closed_display_form');
}


function admin_closed_display_form(){
  $form['back']    = array('#type' => 'submit','#value' => t('Back'));
  return $form;
}


function admin_closed_display_form_submit(&$form_state, $form){
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_admin/closed');
  }
}
