<?php

  /**
 * implementation of prom_crm_my_question
 * Instruction for My_question panel
 */
function prom_crm_my_question($content = NULL) {
  $base_content ='<b>'. t('From here you can see the state of your open questions and review old ones.').'</b><br /> <br />' .
                  t('Go to ') . l(t('Open Question'), 'my_question/open') . t(' to see and manage your open questions;'). '<br />'.
                  t('Go to ') . l(t('Closed Question'), 'my_question/closed') . t(' to view your closed questions.');
  return $base_content;
}


function prom_crm_open_question(){
  global $user;
  $account = $user;
  $header2 = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Open Date'), 'field'=> 'sqldate'));  
  $data2 = array();
  $output;
  $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status = 'open'  AND uid_author_question = %d AND uid_author_last_post != %d", $account->uid, $account->uid));
  if($countrows > 0){
    $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND uid_author_question = %d AND uid_author_last_post != %d" .
             tablesort_sql($header2);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $sql_args = array(
                $account->uid, 
                $account->uid
                );
    $limit = variable_get('prom_crm_rows_question', 10);
    $result = pager_query($query, $limit, 1, $count_query, $sql_args);
    $output .= '<br /><b>' . t('Question waiting for your action: ') .'</b>';
    while($temp2 = db_fetch_array($result)){
      $test=array();
      $data2[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_question/open/' . $temp2['questid'] . '/display'),      
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));
    }
    $output .= theme_table($header2, $data2);
    $output .= theme('pager', NULL, $limit, 1); 

    
  }
  else {
    $output .= '<br />' . '<div>' . t('There is no question waiting for your action.') . '</div><br />';
  }
  return $output;
}


function prom_crm_open_question_assignee(){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Open Date'), 'field'=> 'sqldate')); 
  $data = array();
  $output;
  $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status = 'open' AND uid_author_question = %d AND uid_author_last_post = %d",
                       $account->uid, $account->uid));
  if($countrows > 0){
    $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND uid_author_question = %d AND uid_author_last_post = %d" .
             tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $sql_args = array(
                $account->uid, 
                $account->uid
                );
    $limit = variable_get('prom_crm_rows_question', 10);
    $result = pager_query($query, $limit, 0, $count_query, $sql_args);
    $output .= '<br />' . '<div><b>'.t('Question waiting for the assignee\'s reply:').'</b></div>';
    while($temp2 = db_fetch_array($result)){
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_question/open/' . $temp2['questid'] . '/waiting'),      
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));

    }
    $output .= theme_table($header, $data);
    $output .= theme('pager', NULL, $limit, 0); 
  }
  else{
    $output .= '<br />' . '<div>'.t('There is no question waiting for the assignee\'s reply.').'</div><br />';
  }
  return $output;
}
  
  
/**
 * 
 * implementa tion of closed question with paging of the table
 */
function prom_crm_closed_question(){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Open Date'), 'field'=> 'sqldate'));
  $data = array();
  $output;
  $result;
  $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status = 'closed' AND uid_author_question = %d", $account->uid));
  if($countrows > 0){
    $output .= '<br />' . '<div>'.t('Closed questions: ').' </div>'; 
     
    $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'closed' AND uid_author_question = %d" . tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $sql_args = array(
                $account->uid,
                );
    $limit = variable_get('prom_crm_rows_question', 10);
    $result = pager_query($query, $limit, 0, $count_query, $sql_args);
    while($temp2 = db_fetch_array($result)){
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_question/closed/' . $temp2['questid'] . '/display'),
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));
    }
      $output .= theme_table($header, $data);
      $output .= theme('pager', NULL, $limit, 0); 
      
  }
  else{
    $output .= '<br />' . '<div>'. t('There is no closed question.') . '</div><br />';
  }
  return $output;
}
    
function prom_crm_open_question_display($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account == $user;
  $output;
    $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'open' AND questid = %d", $content);
    $result = db_fetch_array($temp);
    $node = node_load($result['nid'], NULL, TRUE);
    $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
    $array_explode=explodeX(array("#####"),$result['content']);
    $num = 0;
  foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
  return $output . drupal_get_form('prom_crm_open_question_display_form', $content);
}

function prom_crm_open_question_display_form(&$form_state, $arg1){
  $feed = variable_get('prom_crm_feedback', Array('positive, negative'));
  $feed_default = variable_get('prom_crm_feedback_default', $feed[0]);
  $form['quest_id'] = array('#type' => 'hidden', '#value' => $arg1);
  $form['feedback'] = array('#type' => 'select',
                             '#default_value' => $feed_default,
                             '#options'       => $feed
                              );
  $form['close']   = array('#type' => 'submit', '#value' => t('Close the question')); 
  $form['reply']    = array('#type' => 'textarea','#title' => t('Ask for further explaination'),'#required' => FALSE);
  $form['back']    = array('#type' => 'submit','#value' => t('Back'));
  $form['submit']   = array('#type' => 'submit', '#value' => t('Ask for further explanation'));
  return $form;
}

function theme_prom_crm_open_question_display_form($form){
  $output .= '<div><b>'.t('Select a feedback and close the question').'</b></div>';
  $output .= drupal_render($form['feedback']) . drupal_render($form['close']);
  $output .= '<br /><br />' . drupal_render($form['reply']);
  $output .= drupal_render($form['back']);
  $output .= drupal_render($form['submit']);
  $output .= drupal_render($form);
  return $output;
}

function prom_crm_open_question_display_form_submit(&$form_state, $form){
  global $user;
  $account = $user;
  $form_values = $form['values'];
  $mail_send = variable_get('prom_crm_mail_sent', array());
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_question/open');
  } 
  //if the questioner has clicked on 'submit'
  if($form['clicked_button']['#value'] == $form['values']['submit']){
    if ($form_values['reply'] != '' &&  strpos($form_values['reply'], '#####') === FALSE){
      $message = db_result(db_query("SELECT content FROM {prm} WHERE status = 'open' AND questid=%d", $form_values['quest_id']));
      $message .= ' #####QUESTION##### ' . $form_values['reply'];
      $scambio = db_result(db_query("SELECT numscambio FROM {prm} WHERE status = 'open' AND questid=%d", $form_values['quest_id']));
      ++$scambio;
      db_query("UPDATE {prm} SET content = '%s',  tp = %d, numscambio = %d, uid_author_last_post  = uid_author_question WHERE questid = %d", $message, time(), $scambio, $form_values['quest_id'] );
      if ($mail_send['question_reply'] == 'yes'){
        prom_crm_question_reply_mail_send($form_values);
      }
      drupal_set_message(t('The question has been sent.'));
      drupal_goto('my_question/open/waiting');
    }
    else{
      drupal_set_message(t('An error occurred.'), 'error');
    }
  }
  //if the questioner has click on 'close'
  if($form['clicked_button']['#value'] == $form['values']['close']){
    db_query("UPDATE {prm} SET status = 'closed', closedate = %d, tp = %d, feedback = '%s'  WHERE questid = %d", time(), time(), $form_values['feedback'], $form_values['quest_id']);
    if ($mail_send['question_close'] == 'yes'){
      prom_crm_question_close_mail_send($form_values);
    }
    drupal_set_message(t('The question has been closed'));
    drupal_goto('my_question/closed');
  }
}

function prom_crm_question_reply_mail_send($form_values) {
  $module = 'prom_crm';
  $key = 'question_reply';
  $params = $form_values;
  $language = language_default();
  $send = TRUE;
  $assignee = db_result(db_query("SELECT assignee FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  $load_assignee = user_load($assignee);
  $to = $load_assignee->mail;
  $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
}

function prom_crm_question_close_mail_send($form_values) {
  $module = 'prom_crm';
  $key = 'question_close';
  $params = $form_values;
  $language = language_default();
  $send = TRUE;
  $assignee = db_result(db_query("SELECT assignee FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  $load_assignee = user_load($assignee);
  $to = $load_assignee->mail;
  $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
}

function prom_crm_open_question_waiting($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account == $user;
  $output;
  $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'open' AND questid = %d", $content);
  $result = db_fetch_array($temp);
  $node = node_load($result['nid'], NULL, TRUE);
  $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
  $array_explode=explodeX(array("#####"),$result['content']);
  $num = 0;
  foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
  return $output . drupal_get_form('prom_crm_open_question_waiting_form');
}

function prom_crm_open_question_waiting_form(&$form_state){
  $form['back']    = array('#type' => 'submit','#value' => t('Back'));
  return $form;
}

function prom_crm_open_question_waiting_form_submit(&$form_state, $form){
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_question/open/waiting');
  }
}

function closed_question_display ($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account == $user;
  $output;
  $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'closed' AND questid = %d", $content);
  $result = db_fetch_array($temp);
  $node = node_load($result['nid'], NULL, TRUE);
  $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
  $array_explode=explodeX(array("#####"),$result['content']);
  $num = 0;
  foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
  return $output . drupal_get_form('closed_question_display_form');
}

function closed_question_display_form(&$form_state){
  $form['back']    = array('#type' => 'submit','#value' => t('Back'));
  return $form;
}

function closed_question_display_form_submit(&$form_state, $form){
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_question/closed');
  }
}
