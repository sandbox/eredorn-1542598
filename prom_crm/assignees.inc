<?php

/**
 * implementation of _prom_crm_my_answer
 * Instruction for My_answer panel
 */
function _prom_crm_my_answers($content = NULL) {
  $base_content ='<div><b>'. t('Here you can select to manage questions, see the state of your open questions and review old ones.').'</b><br /><br />' .
                  t('Go to ') . l(t('To Assign'), 'my_answer/to_assign') . t(' to select questions you wish to answer to;'). '<br />'.
                  t('Go to ') . l(t('Open Question'), 'my_answer/open') . t(' to see and manage your open questions;'). '<br />'.
                  t('Go to ') . l(t('Closed Question'), 'my_answer/closed') . t(' to view your closed questions.');
  return $base_content;
}


function prom_crm_to_assign_answers($content=NULL){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Open Date'), 'field'=> 'sqldate'));
  $data = array();
  $query = 'vuoto';  
  $limit = variable_get('prom_crm_rows_answer', 10);
  $countrows= db_result(db_query("SELECT COUNT(*) FROM {prm_addressees} WHERE uid = %d", $account->uid));
  if($countrows > 0){
    $result = db_query("SELECT nid FROM {prm_addressees} WHERE uid = %d", $account->uid);
    while($temp = db_fetch_array($result)){
      $countrows2 = db_result(db_query("SELECT COUNT(*) FROM {prm} WHERE status = 'open' AND nid = %d AND assignee=0", $temp['nid']));
      if($countrows2 > 0){
        if($query == 'vuoto'){
          $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND nid = " . $temp['nid'] . " AND assignee=0 ";
        }
        else{
          $query .= " UNION SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND nid = " . "'" . $temp['nid'] . "'" . " AND assignee=0";
        }
      }
    }
  }
  $acc_roles = array_keys($account->roles);
  foreach ($acc_roles as $role){  
    foreach (node_get_types() as $type => $name) {
      $var_glob = variable_get('prom_crm_answers-' . $type, array());
      if($var_glob[$role] == 1){
        $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status = 'open' AND type = '%s' AND assignee=0", $name->type));
        if($countrows > 0){
          if($query == 'vuoto'){
            $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND type = " . "'" . $name->type . "'" . " AND assignee=0 ";
          }
          else{
            $query .= " UNION SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND type = " . "'" . $name->type . "'" . " AND assignee=0 ";
          }
        }
      }
    }
  }
  if ($query != 'vuoto'){
    $query .= tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $sql_args = array();
    $result = pager_query($query, $limit, 0, $count_query, $sql_args);  
    while($temp2 = db_fetch_array($result)){
      $test=array();
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_answer/to_assign/' . $temp2['questid'] . '/display'),
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));
    }
    $output .= theme_table($header, $data);
    $output .= theme('pager', NULL, $limit, 0); 
  }
  if(empty($data)){
    return '<strong>' . t('There is no question to assign') . '</strong>';
  }
  
  return $output;
}


//page callback of to_assign/%/display
function _prom_crm_menu_page($content = NULL, $arg1 = NULL, $arg2 = NULL) {
  global $user;
  $account = $user;
  $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'open' AND questid = %d", $content);
  $result = db_fetch_array($temp);
  $node = node_load($result['nid'], NULL, TRUE);
  $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br /><div><b>Content: </b>' . $node->body .  '</div><br /><div><b>Question: </b>' . $result['content'] . '</div><br />'; 
  return $output . drupal_get_form('prom_crm_quest_id_form', $content);
}


/**
 * 
 * Form di _prom_crm_menu_page
 * @param unknown_type $form_state
 * @param unknown_type $arg1
 */
function prom_crm_quest_id_form(&$form_state, $arg1){
  $form['quest_id'] = array('#type' => 'hidden', '#value' => $arg1);
  $form['back'] = array('#type' => 'submit', '#value' => t('Back'));
  $form['submit'] = array('#type' => 'submit', '#value' => t('Manage this question'));
  return $form;
}


function theme_prom_crm_quest_id_form($form){
  return;
}


function prom_crm_quest_id_form_submit(&$form_state, $form){
  global $user;
  $account = $user;
  $form_values = $form['values'];
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_answer/to_assign');
  }
  else {
    if(is_null(module_load_include('inc','prom_crm', 'prom_crm_overload'))){ 
      module_load_include('inc','prom_crm_overload');
      crm_overload_quest_id_submit(&$form_state, $form);
    }else{
      db_query("UPDATE {prm} SET assignee = %d, tp = %d WHERE questid = %d", $account->uid, time(), $form_values['quest_id'] );
      $message = t('The question has been assigned to you, go to ') . l(t('my_answer/open'), 'my_answer/open'). t(' and leave the <b>first</b> answer');
      drupal_set_message($message);
      $mail_send = variable_get('prom_crm_mail_sent', array());
      if($mail_send['assignee_manage'] == 'yes'){
        prom_crm_assignee_manage_mail_send($form_values);
      }      
      drupal_goto('my_answer/open');
    }
  }
}

function prom_crm_assignee_manage_mail_send($form_values) {
  $module = 'prom_crm';
  $key = 'assignee_manage';
  $params = $form_values;
  $language = language_default();
  $send = TRUE;
  $assignee = db_result(db_query("SELECT assignee FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  $load_assignee = user_load($assignee);
  $to = $load_assignee->mail;
  $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
}

function prom_crm_open_answers($content=NULL){ 
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Open Date'), 'field'=> 'sqldate'));  $data = array();
  $output;
  $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status = 'open' AND assignee = %d  AND uid_author_question = uid_author_last_post", $account->uid));
  if($countrows > 0){
    $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND assignee = %d AND uid_author_question = uid_author_last_post" .
             tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $sql_args = array(
                      $account->uid
                      );
    $limit = variable_get('prom_crm_rows_answer', 10);
    $result = pager_query($query, $limit, 0, $count_query, $sql_args);
    $data2 = array();
    $output .= '<br />' . '<div>'.t('Question waiting for your reply: ').'</div>';
    while($temp2 = db_fetch_array($result)){
      $test=array();
      $data2[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_answer/open/' . $temp2['questid'] . '/display'),      
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));
    }
    $output .= theme_table($header, $data2);
    $output .= theme('pager', NULL, $limit, 0); 
  }
  else {
    $output .= '<br />' . '<div>'.t('There is no question waiting for your reply.') .'</div><br />';
  }
  return $output;
}


/**
 * 
 * implementing tab_menu for open answers
 */
function prom_crm_open_answers_assignee($content = NULL){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Open Date'), 'field'=> 'sqldate'));  $data = array();
  $output;
  $countrows= db_result(db_query("SELECT COUNT(questid) FROM {prm} WHERE status = 'open' AND assignee = %d AND assignee = uid_author_last_post", $account->uid));
  if($countrows > 0){
    $data = array();
    $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'open' AND assignee = %d AND assignee = uid_author_last_post" .
              tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $sql_args = array(
                      $account->uid
                      );
    $limit = variable_get('prom_crm_rows_answer', 10);
    $result = pager_query($query, $limit, 0, $count_query, $sql_args);
    $output .=  '<br />' . '<div>'.t('Question waiting for the questioner\'s action: ').' </div>';
    while($temp2 = db_fetch_array($result)){
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_answer/open/' . $temp2['questid'] . '/waiting'),
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));
    }
    $output .= theme_table($header, $data);
    $output .= theme('pager', NULL, $limit, 0); 
  }
  else{
    $output .= '<br />' .  '<div>'.t('There is no question waiting for the questioner\'s action.').'</div><br />';
  } 
  return $output;
}


function prom_crm_open_answer_display($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account == $user;
  $output;
  $temp = db_query("SELECT nid, type, title, content FROM {prm} WHERE status = 'open' AND questid = %d", $content);
  $result = db_fetch_array($temp);
  $node = node_load($result['nid'], NULL, TRUE);
  $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
  $array_explode=explodeX(array("#####"),$result['content']);
  $num = 0;
  foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
  return $output . drupal_get_form('prom_crm_open_answer_display_form', $content);  
}


/**
 * Form of prom_crm_open_answer_display
 */
function prom_crm_open_answer_display_form(&$form_state, $arg1){
  $form['quest_id'] = array('#type' => 'hidden', '#value' => $arg1);
  $form['reply']    = array('#type' => 'textarea','#title' => t('Reply'),'#required' => FALSE);
  $form['back']     = array('#type' => 'submit', '#value' => t('Back'));
  $form['submit']   = array('#type' => 'submit', '#value' => t('Reply to the question'));
  return $form;
}


function prom_crm_open_answer_display_form_submit(&$form_state, $form){
  global $user;
  $account = $user;
  $form_values = $form['values'];
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_answer/open');
  }
  else{
    if ($form_values['reply'] != '' &&  strpos($form_values['reply'], '#####') === FALSE){
      $message = db_result(db_query('SELECT content FROM {prm} WHERE questid=%d', $form_values['quest_id']));
      $message .= ' #####REPLY##### ' . $form_values['reply'];
      $scambio = db_result(db_query('SELECT numscambio FROM {prm} WHERE questid=%d', $form_values['quest_id']));
      ++$scambio;
      db_query("UPDATE {prm} SET content = '%s',  uid_author_last_post  = assignee, tp = %d, numscambio = %d WHERE questid = %d", $message, time(), $scambio, $form_values['quest_id'] );
      $mail_send = variable_get('prom_crm_mail_sent', array());
      if ($mail_send['assignee_reply'] == 'yes'){
        prom_crm_assignee_reply_mail_send($form_values);
      }
      drupal_set_message(t('The answer has been sent.'));
      drupal_goto('my_answer/open/waiting');
    }
    else{
      drupal_set_message(t('An error occurred.'), 'error');
    }
  }
}


function prom_crm_assignee_reply_mail_send($form_values) {
  $module = 'prom_crm';
  $key = 'assignee_reply';
  $params = $form_values;
  $language = language_default();
  $send = TRUE;
  $questioner = db_result(db_query("SELECT uid_author_question FROM {prm} WHERE questid = %d", $form_values['quest_id']));
  $load_questioner = user_load($questioner);
  $to = $load_questioner->mail;
  $result1 = drupal_mail($module, $key, $to, $language, $params, $from, $send);
}

function theme_prom_crm_open_answer_display_form($form){
  $output .= drupal_render($form['reply']);
  $output .= drupal_render($form['back']);
  $output .= drupal_render($form['submit']);
}


/**
 * function prom_crm_open_answer_display
 * page callback di myanswer/open/%/display
 * Enter description here ...
 * @param unknown_type $form_state
 * @param unknown_type $arg1
 */
function prom_crm_open_answer_waiting($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account == $user;
  $output;
  $temp = db_query("SELECT nid, type, title, content FROM {prm} where status = 'open' AND questid = %d", $content);
  $result = db_fetch_array($temp);
  $node = node_load($result['nid'], NULL, TRUE);
  $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
  $array_explode=explodeX(array("#####"),$result['content']);
  $num = 0;
  foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
  return $output . drupal_get_form('open_answer_waiting_form');
}


function open_answer_waiting_form(){
  $form['back']    = array('#type' => 'submit','#value' => t('Back'));
  return $form;
}


function open_answer_waiting_form_submit(&$form_state, $form){
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_answer/open');
  }
}


function prom_crm_closed_answers($content=NULL){
  global $user;
  $account = $user;
  $header = array(array('data'=>t('Question Id'), 'field' =>'questid', 'sort' => 'asc'),
  array('data' => t('Content Type'), 'field' => 'type'),
  array('data' => t('Title'), 'field' => 'title'), 
  array('data' => t('Question'), 'field' => 'content'), 
  array('data' => t('Open Date'), 'field'=> 'sqldate'));  $data = array();
  $output;
  $countrows= db_result(db_query("SELECT COUNT(*) FROM {prm} WHERE status = 'closed' AND assignee = %d", $account->uid));
  if($countrows > 0){
    $query = "SELECT questid, type, title, content, sqldate FROM {prm} WHERE status = 'closed' AND assignee = %d" .
             tablesort_sql($header);
    $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
    $sql_args = array(
                $account->uid
                );
    $limit = variable_get('prom_crm_rows_answer', 10);
    $result = pager_query($query, $limit, 0, $count_query, $sql_args);
    $output .= '<br />' . '<div>'.t('Closed questions: ').' </div>';
    while($temp2 = db_fetch_array($result)){
      $data[] = array($temp2['questid'],
      $temp2['type'],
      $temp2['title'],
      l($temp2['content'],'my_answer/closed/' . $temp2['questid'] . '/display'),    
      format_date($temp2['sqldate'],'custom', 'd/m/Y - G:i'));
    }
    $output .= theme_table($header, $data);
    $output .= theme('pager', NULL, $limit, 0);
    
  }
  else{
    $output .= '<div>'.t('There is no closed question.').'</div><br />';
  }
  return $output;
}


function closed_answer_display ($content = NULL, $arg1 = NULL, $arg2 = NULL){
  global $user;
  $account == $user;
  $output;
  $temp = db_query("SELECT nid, type, title, content FROM {prm} WHERE status = 'closed' AND questid = %d", $content);
  $result = db_fetch_array($temp);
  $node = node_load($result['nid'], NULL, TRUE);
  $output .= '<br /><div><b>Content type: </b>' . $result['type']. '</div><br /><div><b> Title: </b>' . $result['title'] . '</div><br />'.'<div><b>Content: </b>' . $node->body .  '</div><br />';
  $array_explode=explodeX(array("#####"),$result['content']);
  $num = 0;
  foreach ($array_explode as $exploded){
    if($num==0){
      $output .= '<b>Question: </b>' . $exploded . '<br />';
    }
    elseif($exploded == 'REPLY'){
      $output .= '<ul><li><b>' . t('Reply') .': </b>' . $array_explode[$num+1] . '</li></ul><br /><br />';
    }
    elseif ($exploded == 'QUESTION'){
      $output .= '<b>'.t('Further Information').': </b>' . $array_explode[$num+1].'<br />'; 
    }
    elseif ($exploded == 'ADMIN'){
      if($array_explode[$num-2] == 'QUESTION'){
        $output .= '<br />';
      }
      $output .= '<b>'. t('ADMIN REPLY').': </b>'. $array_explode[$num+1] .'<br /><br />';
    }
    ++$num;
  }
  return $output . drupal_get_form('closed_answer_display_form');
}


function closed_answer_display_form(){
  $form['back']    = array('#type' => 'submit','#value' => t('Back'));
  return $form;
}


function closed_answer_display_form_submit(&$form_state, $form){
  if($form['clicked_button']['#value'] == $form['values']['back']){
    drupal_goto('my_answer/closed');
  }
}

